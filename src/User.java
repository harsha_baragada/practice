public class User {

        private int id;
        private String name;
        private double age;
        private char[] chars;
        int price;

        public static final String COMPANY_NAME ="MPP";

    public User() {

        System.out.println("User has been initialized");
    }

    public User(int id, String name, double age, char[] chars) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.chars = chars;
    }

    public String sayHelloWorld() {
        System.out.println("");

        int number= this.id;

        return "Hello world"+number;
    }


}
