package example.polimorphism;

public class Deer extends Animal implements Vegetarian {

    public void property() {
        System.out.println("Deer is a vegetarian");
    }
}
