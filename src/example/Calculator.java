package example;

public class Calculator {


    public int addition(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int multiplication(int a, int b) {
        return a * b;
    }

    public int division(int a, int b) {
        return a / b;
    }

    public int modulus(int a, int b) {
        return a % b;
    }


    public int increment(int a) {
        return ++a;
    }

    public int decrement(int a) {
        return --a;
    }
}
