package example.encapsulation;

public class Person {

    private String name;
    private String lastName;
    private int id;
    private double weightInKgs;
    private double heightInCms;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getWeightInKgs() {
        return weightInKgs;
    }

    public void setWeightInKgs(double weightInKgs) {
        this.weightInKgs = weightInKgs;
    }

    public double getHeightInCms() {
        return heightInCms;
    }

    public void setHeightInCms(double heightInCms) {
        this.heightInCms = heightInCms;
    }
}
