package example.inheritance;

public class Animal {

    private String typeOfTheAnimal;

    public Animal() {
        System.out.println("The animal class has been invoked");
    }

    public Animal(String typeOfTheAnimal) {
        this.typeOfTheAnimal = typeOfTheAnimal;

        System.out.println("The animal here is "+typeOfTheAnimal);
    }

    public void eat(){
        System.out.println("The animal is eating");
    }

    public void walk(){
        System.out.println("The animal has been walking");
    }
}
