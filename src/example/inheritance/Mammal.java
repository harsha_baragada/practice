package example.inheritance;

public class Mammal extends Animal{

    private String mammalType;

    public Mammal(String mammalType) {
        this.mammalType = mammalType;
        System.out.println("The mammal is "+mammalType);
    }

    public Mammal() {
        System.out.println("This is mammal class");
    }
}
