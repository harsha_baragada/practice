package example.inheritance;

public class Dog extends Mammal {

    private String name;

    public Dog(String name) {
        super("some mammal");
        this.name = name;
        System.out.println("A dog with name " + name + " has been created");
        bark();
    }

    public void bark() {
        System.out.println("The dog is barking");
    }


    public void eat() {
        System.out.println(name + " The dog is eating");
    }
}
