package example.overriding;

public class MethodOverriding extends OverridingParent {

    @Override
    public void print() {
        System.out.println("Hello I am overridden method in child");
    }
}
