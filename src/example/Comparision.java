package example;


/*
 * This class will provide us examples for all relational operators
 * */
public class Comparision {

    public boolean isEquals(int a, int b) {

        return a == b;
    }

    public boolean isNotEquals(int a, int b) {

        return a != b;
    }

    public boolean isGreater(int a, int b) {

        return a > b;
    }

    public boolean isLesser(int a, int b) {

        return a < b;
    }

    public boolean isGreaterOrEqual(int a, int b) {

        return a >= b;
    }

    public boolean isLesserOrEqual(int a, int b) {

        return a <= b;
    }



    public boolean logicalAnd(boolean a, boolean b){
        return a && b;
    }


    public boolean logicalOr(boolean a, boolean b){
        return a || b;
    }

    public boolean logicalNot(boolean a){
        return !a ;
    }

}
