package example.datastructures;

import java.util.Stack;

public class StackExample {

    Stack stack = new Stack();

    public void stackExample(){

        stack.add("first element");
        stack.add("second element");
        stack.add("third element");
        stack.add("fourth element");
        stack.add("fifth element");

        System.out.println( stack.peek());

    }
}
