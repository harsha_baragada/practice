package example.datastructures;

import java.util.BitSet;

public class BitSetExample {

    public void bitSetExample() {

        BitSet bits1 = new BitSet(16);
        BitSet bits2 = new BitSet(16);


        for (int i = 0; i < 16; i++) {
            if ((i % 2) == 0) bits1.set(i);
            if ((i % 5) != 0) bits2.set(i);
        }


        System.out.println("Initial pattern of bits "+ bits1);
        System.out.println("Initial pattern of bits "+ bits2);

        // And operation
        bits2.and(bits1);
        System.out.println(bits2);

        //Or operation
        bits2.or(bits1);
        System.out.println(bits2);

    }
}
