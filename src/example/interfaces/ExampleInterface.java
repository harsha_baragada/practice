package example.interfaces;

public interface ExampleInterface extends Interface1, Interface2{

    String name = "peter";

    String sayHelloWorld();
}
