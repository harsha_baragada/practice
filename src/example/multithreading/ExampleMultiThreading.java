package example.multithreading;

public class ExampleMultiThreading {



    public static void main(String[] args) {
        ExamplePrint examplePrint = new ExamplePrint();
        ThreadDemo threadDemo = new ThreadDemo("Bombay", examplePrint);
        ThreadDemo threadDemo1 = new ThreadDemo("Newyork", examplePrint);
        threadDemo.start();
        threadDemo1.start();
    }

}


class ExamplePrint{

    boolean flag = true;
    public synchronized void printCount(){

        if(flag) {
            try {
                for (int i = 5; i > 0; i--) {
                    System.out.println("Counter " + i);
                    wait(1000);
                }

            } catch (Exception e) {
                System.out.println("The thread is interrupted");
            }
        }
        flag = true;
        notify();
    }
}

class ThreadDemo extends Thread{
    private Thread t;
    private String threadName;
    private ExamplePrint  examplePrint;

    public  ThreadDemo(String threadName, ExamplePrint examplePrint) {
        this.threadName = threadName;
        this.examplePrint = examplePrint;
        System.out.println("Creating thread "+threadName);
    }

    @Override
    public void run() {
            examplePrint.printCount();
        System.out.println("The thread is finished "+threadName);
    }

    public void start(){
        System.out.println("Starting the thread "+threadName);
        if(t==null){
            t = new Thread(this,threadName);
            t.start();
        }
    }
}

