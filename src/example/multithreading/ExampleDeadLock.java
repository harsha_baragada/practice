package example.multithreading;

public class ExampleDeadLock {

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();
    public static void main(String[] args ){
        ThreadDemo1 threadDemo1 = new ThreadDemo1();
        ThreadDemo2 threadDemo2 = new ThreadDemo2();
        threadDemo1.start();
        threadDemo2.start();

    }
    private static class ThreadDemo1 extends Thread{
        public void run(){
            synchronized (lock1){
                System.out.println("Thread 1 holding the lock 1");
                try{
                    Thread.sleep(10);

                }
                catch (InterruptedException e){
                    System.out.println(e);
                }
                System.out.println("Thread 1 waiting for lock 2");
                synchronized (lock2){
                    System.out.println("Thread 1 holding lock1 and lock 2");
                }
            }

        }
    }

    private static class ThreadDemo2 extends Thread{
        public void run(){
            synchronized (lock1){
                System.out.println("Thread 2 holding the lock2");
                try{
                    Thread.sleep(10);

                }
                catch (InterruptedException e){
                    System.out.println(e);
                }
                System.out.println("Thread 2 waiting for lock 1");
                synchronized (lock2){
                    System.out.println("Thread 2 holding lock1 and lock 2");
                }
            }

        }
    }
}

