package example.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExampleCollections {

    Collection fruits = new ArrayList<String>();
    public Collection<String> getAllFruits(){
        fruits.add("Apple");
        fruits.add("Mango");
        fruits.add("Banana");
        fruits.add("Papaya");
        fruits.add("Water melon");
        fruits.add("Pears");
        fruits.add("Cherry");
        return fruits;
    }

}
