package example.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueueInterface {

    public Queue<String> getQueue(){

        Queue<String> names = new PriorityQueue<String>();
        System.out.println( names.add("Peter"));
        names.add("Parker");
        names.add("Bob");
        names.add("Eva");
        names.add("Mark");
        names.add("Katherine");
        names.add("Arva");
        System.out.println(names.remove("Katherine"));
        return names;
    }

}
