package example.collections.set;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class ExampleLinkedHashSet {

    public void getLinkedHashSet(){
        Collection<String> strings = new LinkedList<String>();
        strings.add("P");
        strings.add("A");
        strings.add("R");
        strings.add("R");
        strings.add("O");
        strings.add("T");
        System.out.println(strings);
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>(strings);
        System.out.println(linkedHashSet);
    }
}
