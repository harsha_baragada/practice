package example.collections.set;

import java.util.HashSet;
import java.util.Set;

public class ExampleSetInterface {

    Set<Integer>integers = new HashSet<Integer>();
    public Set<Integer> getIntegers(){

        integers.add(2);
        integers.add(5);
        integers.add(36);
        integers.add(5);
        integers.add(3);
        integers.add(85);
        integers.add(345);
        return integers;
    }
}
