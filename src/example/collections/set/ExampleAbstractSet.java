package example.collections.set;

import javax.xml.stream.events.Characters;
import java.util.AbstractSet;
import java.util.HashSet;

public class ExampleAbstractSet {

    AbstractSet<Character> chars = new HashSet<Character>();

    public AbstractSet<Character> printCharacters(){

        chars.add('a');
        chars.add('b');
        chars.add('b');
        chars.add('b');
        chars.add('b');
        return chars;
    }
}
