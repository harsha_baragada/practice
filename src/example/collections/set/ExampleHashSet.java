package example.collections.set;

import java.util.HashSet;

public class ExampleHashSet {
    HashSet<String> hashSet = new HashSet<String>();

    public HashSet<String> getHashSet() {
        hashSet.add("B");
        hashSet.add("J");
        hashSet.add("L");
        hashSet.add("P");
        hashSet.add("M");
        hashSet.add("Y");
        hashSet.add("M");
        return hashSet;
    }
}
