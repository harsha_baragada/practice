package example.collections.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public class ExampleSortedSetAndTreeSet {

    public void getSortedSet(){
        SortedSet<Integer> sortedSet = new TreeSet<Integer>();
        sortedSet.add(5);
        sortedSet.add(575);
        sortedSet.add(47);
        sortedSet.add(4745);
        sortedSet.add(85);
        sortedSet.add(858);
        sortedSet.add(45);
        sortedSet.add(5435);
        sortedSet.add(534);
        System.out.println(sortedSet);

        Collection<Integer> integers = new ArrayList<Integer>();
        integers.add(5);
        integers.add(2);
        integers.add(4);
        integers.add(5);
        integers.add(2);
        integers.add(8);
        integers.add(9);
        integers.add(3);
        integers.add(9);
        System.out.println(integers);
        //eleminate duplicates and sort in ascending order
        System.out.println(new TreeSet<Integer>(integers));

    }
}
