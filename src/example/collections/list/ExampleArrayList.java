package example.collections.list;

import java.util.ArrayList;

public class ExampleArrayList {

    public ArrayList<String> getArrayList() {
        ArrayList<String> cities = new ArrayList<String>();
        cities.add("Mumbai");
        cities.add("Pune");
        cities.add("Hyderabad");
        cities.add("Chennai");
        cities.add("Delhi");
        ArrayList<String> transferredCities = new ArrayList<String>(cities);
        return transferredCities;


    }
}
