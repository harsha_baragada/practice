package example.collections.list;

import java.util.ArrayList;
import java.util.List;

public class ExampleListInterface {
    List<String> strings = new ArrayList<String>();

    public List<String> getStrings() {

        strings.add("peter");
        strings.add("bob");
        strings.add("eva");
        strings.add("emile");
        strings.add("david");
        strings.add("john");
        return strings;
    }
}
