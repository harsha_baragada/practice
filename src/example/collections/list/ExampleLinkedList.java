package example.collections.list;

import java.util.LinkedList;

public class ExampleLinkedList {

    LinkedList<String> strings = new LinkedList<String>();

    public LinkedList<String> getStrings(){

        LinkedList<String> strings = new LinkedList<String>();
        strings.add("A");
        strings.add("B");
        strings.add("C");
        strings.add("D");
        strings.add("E");
        strings.addLast("G");
        strings.addFirst("H");
        strings.add(4,"I");
        return strings;

    }
}
