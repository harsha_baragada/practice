package example.collections.map;

import java.util.SortedMap;
import java.util.TreeMap;

public class ExampleSortedMap {

    public void getFruits() {
        SortedMap<Integer, String> fruits = new TreeMap<Integer, String>();
        fruits.put(2, "Apple");
        fruits.put(3, "Pears");
        fruits.put(1, "Banana");
        fruits.put(9, "Mango");
        fruits.put(4, "Cherry");
        System.out.println(fruits);
        System.out.println(fruits.lastKey());
        System.out.println(fruits.firstKey());

    }
}
