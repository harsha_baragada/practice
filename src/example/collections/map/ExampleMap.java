package example.collections.map;

import java.util.HashMap;
import java.util.Map;

public class ExampleMap {

    public void getMap(){

        Map<Integer,String> integerStringMap = new HashMap<Integer, String>();
        integerStringMap.put(1,"Peter");
        integerStringMap.put(5,"Eva");
        integerStringMap.put(9,"Parker");
        integerStringMap.put(6,"Emile");
        integerStringMap.put(7,"David");
        System.out.println(integerStringMap.remove(9));
        System.out.println(integerStringMap);
    }
}
