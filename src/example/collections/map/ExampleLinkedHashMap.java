package example.collections.map;

import java.util.LinkedHashMap;

public class ExampleLinkedHashMap {

    public void printLinkedHashMap() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(5,0.75f,true);
        linkedHashMap.put(2,"Apple");
        linkedHashMap.put(3,"Pears");
        linkedHashMap.put(1,"Banana");
        linkedHashMap.put(9,"Mango");
        linkedHashMap.put(4,"Cherry");
        System.out.println(linkedHashMap);
        linkedHashMap.put(2,"Apple");
        System.out.println(linkedHashMap.get(4));
        System.out.println(linkedHashMap.get(9));

        System.out.println(linkedHashMap);


    }
}
