package example.collections.map;

import java.util.Map;
import java.util.WeakHashMap;

public class
ExampleWeakHashMap {
    public void getWeakHashMap() {
        Map weakHashMap = new WeakHashMap<Integer, String>();
        weakHashMap.put(2, "Mumbai");
        weakHashMap.put(4, "Hyderabad");
        weakHashMap.put(7, "Delhi");
        weakHashMap.put(9, "Culcatta");
        System.out.println(weakHashMap);
        System.out.println(weakHashMap.get(4));
        System.out.println(weakHashMap.get(9));
        System.out.println(weakHashMap);
    }

}
